function lf=lagrangeRemainder_dynamicsClosedLoopLinear(x,u,dx,du)

lf=interval();

lf(1,1)=0;
lf(2,1)=0;
lf(3,1)=- dx(2)*((dx(1)*sin(x(2)))/2 + (dx(2)*x(1)*cos(x(2)))/2) - (dx(1)*dx(2)*sin(x(2)))/2;
lf(4,1)=dx(2)*((dx(1)*cos(x(2)))/2 - (dx(2)*x(1)*sin(x(2)))/2) + (dx(1)*dx(2)*cos(x(2)))/2;
lf(5,1)=0;
lf(6,1)=0;
lf(7,1)=0;
lf(8,1)=0;
lf(9,1)=0;
lf(10,1)=0;
